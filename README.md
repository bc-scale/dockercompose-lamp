# DockerCompose-LAMP

Simplesmente uma estrutura para você implementar um servidor LAMP para produção

# Pré-requisitos:


- docker 20.xx.xx
  
- docker-compose 1.29.2
  

# Execução:

```
docker-compose -up -d // para iniciar os conteiners
```

```
docker-compose down // para desligar os conteiners
```

## Informações:

- Todo codigo deve ser inserido na pasta **/www/**
  
- Os aquivos do banco serão salvos na pasta **/data**
  
- Caso queira uma configuração mais expecifica, altere o arquivo **.env**
  
- Os DockerFiles estão dentro das pastas **/bin/**
  
- Caso queira iniciar um conteiner com as tabelas já inicializadas, adicione um arquivo **.sql** dentro de **/bin/mariadb**
  
- O servidor web vai estar ligado na porta 80, para alterar isso mexa no **.env**
  
- o PhpMyAdmin, está na porta 8080
